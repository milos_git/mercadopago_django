const productDescription = document.getElementById("product-description");

const beer3Button = document.getElementById("donate-3");
const beer5Button = document.getElementById("donate-5");
const beer10Button = document.getElementById("donate-10");

const amountInput = document.getElementById("amount-input");
const totalAmount = document.getElementById("total-amount");

const apiKey = 'TEST-5945344861045619-061103-2d286fd0f1b7767a133488e63b3d4ba7-364838279'; // Your API key

let beerCount = 0;

beer3Button.addEventListener("click", () => {
  amountInput.value = 3;
  beerCount = 3;
  updateTotalAmount();
});

beer5Button.addEventListener("click", () => {
  amountInput.value = 5;
  beerCount = 5;
  updateTotalAmount();
});

beer10Button.addEventListener("click", () => {
  amountInput.value = 10;
  beerCount = 10;
  updateTotalAmount();
});

amountInput.addEventListener("input", () => {
  beerCount = amountInput.value;
  updateTotalAmount();
});

const updateTotalAmount = () => {
  const updatedAmount = beerCount * 50;
  totalAmount.innerText = updatedAmount;
};

//MP
const mercadopago = new MercadoPago("TEST-9bea6bdf-ed6f-4925-9051-acf100448886", {
  locale: "es-MX", // The most common are: 'pt-BR', 'es-AR' and 'en-US'
});

document.getElementById("checkout-btn").addEventListener("click", function () {
  const orderData = {
    quantity: 1,
    description: productDescription.innerText,
    price: totalAmount.innerText,
  };

  console.log("Total Amount:", totalAmount.innerText);
  console.log("Total Amount:", typeof totalAmount.innerText);

  console.log("Request Body:", JSON.stringify(orderData));
  

  //fetch("http://localhost:8080/create_preference", {
  fetch("http://localhost:8000/create_preference", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      'Authorization': `Bearer ${apiKey}`
    },
    body: JSON.stringify(orderData),
    

  })
    .then(function (response) {

      console.log("Response:", response);
      return response.json();
    })
    .then(function (preference) {
      createCheckoutButton(preference.id);
    })
    .catch(function () {
      alert("Unexpected Fail!!!");
    });
});

function createCheckoutButton(preferenceId) {
  // Initialize the checkout
  const bricksBuilder = mercadopago.bricks();

  const renderComponent = async (bricksBuilder) => {
    if (window.checkoutButton) window.checkoutButton.unmount();

    await bricksBuilder.create(
      "wallet",
      //"button-checkout", // class/id where the payment button will be displayed
      "wallet_container", // class/id where the payment button will be displayed
      {
        initialization: {
          preferenceId: preferenceId,
        },
        callbacks: {
          onError: (error) => console.error(error),
          onReady: () => {},
        },
      }
    );
  };
  window.checkoutButton = renderComponent(bricksBuilder);
}
