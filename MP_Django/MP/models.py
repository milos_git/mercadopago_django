from django.db import models

class User(models.Model):
    username = models.CharField(max_length=100)
    email = models.EmailField(unique=True)

class Payment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    description = models.CharField(max_length=255)
    price = models.FloatField()
    quantity = models.IntegerField()
    date = models.DateTimeField(auto_now_add=True)
