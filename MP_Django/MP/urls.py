from django.urls import path
from . import views
from . views import *


urlpatterns = [
    
    path('', home, name='home'),

    # path('', views.index, name='index'),
    path('create_preference', views.create_preference, name='create_preference'),
    path('feedback', views.feedback, name='feedback'),
]