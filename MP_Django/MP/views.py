from django.shortcuts import render
from .models import User, Payment
import json

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from mercadopago import SDK
import traceback

sdk = SDK("TEST-5945344861045619-061103-2d286fd0f1b7767a133488e63b3d4ba7-364838279")

@csrf_exempt
def home(request):
    return render(request, 'index.html')

@csrf_exempt
def index(request):
    return JsonResponse({"message": "Hello, world!"})

@csrf_exempt
def create_preference(request):
    if request.method == 'POST':
        try:
            # data = request.POST
            # print("Request Data:", data)  # Log request data for debugging purpose
            # description = data.get('description')
            # price = data.get('price')
            # #print(price)
            # quantity = int(data.get('quantity'))

            data = json.loads(request.body)
            
            # Extract the fields from the parsed JSON data
            quantity = int(data.get('quantity'))
            description = data.get('description')
            price = float(data.get('price'))

            preference = {
                "items": [
                    {
                        "title": description,
                        "unit_price": price,
                        "quantity": quantity
                    }
                ],
                "back_urls": {
                    "success": "http://localhost:8000",
                    "failure": "http://localhost:8000",
                    "pending": ""
                },
                "auto_return": "approved"
            }

            preference_response = sdk.preference().create(preference)
            preferences = preference_response["response"]
            print("sss")
            print(preferences)
            preference_id = preferences['id']
            return JsonResponse({"id": preference_id}, status=200)
        except Exception as e:
            traceback.print_exc()
            return JsonResponse({"error": "Unexpected Fail!!"}, status=500)
    else:
        return JsonResponse({"error": "Method not allossswed"}, status=405)

@csrf_exempt
def feedback(request):
    if request.method == 'GET':
        payment_id = request.GET.get('payment_id')
        status = request.GET.get('status')
        merchant_order_id = request.GET.get('merchant_order_id')
        return JsonResponse({"Payment": payment_id, "Status": status, "MerchantOrder": merchant_order_id}, status=200)
    else:
        return JsonResponse({"error": "Method not allowed"}, status=405)
# views.py


# @csrf_exempt
# def create_preference(request):
#     if request.method == 'POST':
#         try:
#             data = request.POST
#             username = data.get('username')  # Assuming you have a field for username in your request
#             email = data.get('email')  # Assuming you have a field for email in your request
#             description = data.get('description')
#             price = float(data.get('price'))
#             quantity = int(data.get('quantity'))

#             # Get or create user
#             user, created = User.objects.get_or_create(username=username, email=email)

#             # Create payment
#             payment = Payment.objects.create(
#                 user=user,
#                 description=description,
#                 price=price,
#                 quantity=quantity
#             )

#             # Your preference creation logic goes here...

#             return JsonResponse({"id": payment.id}, status=200)
#         except Exception as e:
#             traceback.print_exc()
#             return JsonResponse({"error": "Unexpected error"}, status=500)
#     else:
#         return JsonResponse({"error": "Method not allged"}, status=405)
